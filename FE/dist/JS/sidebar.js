var sidebar = document.getElementById('sidebar');
var openSidebar = document.getElementById('menu-button');
var overlay = document.getElementsByClassName('dark-overlay')[0];

sidebar.addEventListener('click', (e) => {
  console.log(e.target);
});

openSidebar.addEventListener('click', (e) => {
  console.log(e.target);
  sidebar.classList.remove('hide-menu');
  // overlaySidebar.classList.remove('hide-menu');
  sidebar.classList.add('show-menu');
  // overlaySidebar.classList.add('show-menu');
  overlay.classList.remove('lighten');
  overlay.classList.add('darken');
});

window.addEventListener('click', (e) => {
  var check = sidebar.contains(e.target);
  if (!check && !e.target.contains(openSidebar)) {
    sidebar.classList.remove('show-menu');
    console.log(sidebar.classList);
    sidebar.classList.add('hide-menu');
    overlay.classList.remove('darken');
    overlay.classList.add('lighten');
  }
});
